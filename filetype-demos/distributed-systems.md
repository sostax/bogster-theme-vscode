# New note

- this is a list 
- in a markdown file



[ ] this is a checkbox in a markdown file

## Heading level 2

### Heading level 3

**bold text**

*italic text*

~~strike through~~


> block quote
> 
> where you put the things someone else said.
>
> Okay?

#### A Level 4 header and a code block

```python
if this is that:
    my_func(do.things)
else:
    print("nothing")
```

[This is a link](https://duckduckgo.com)