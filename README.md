# Bogster: A dark theme for Visual Studio Code

Based primarily on the [Bogster theme for Vim by vv9k](https://github.com/vv9k/bogster) with inspiration from [Oceanic Park by seanonthenet](https://github.com/seanonthenet/Oceanic-Park) 

TODO:

[ ] finish this README

[ ] menu borders

[ ] git decor

[ ] bottom panel styling